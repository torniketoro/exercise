<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\UsersController;
use App\Http\Controllers\Api\CartsController;
use App\Http\Controllers\Api\AlgorithmsController;

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'users'], static function () {
    Route::get('{id}', [UsersController::class, 'show']);
});

Route::group(['prefix' => 'carts'], static function () {
    Route::get('/', [CartsController::class, 'index']);
    Route::get('joined', [CartsController::class, 'indexJoined']);
    Route::post('{product_id}', [CartsController::class, 'create']);
    Route::delete('{product_id}', [CartsController::class, 'delete']);
    Route::patch('set-quantity/{product_id}', [CartsController::class, 'setQuantity']);
});

Route::group(['prefix' => 'algorithms'], static function () {
    Route::get('find-missing-value', [AlgorithmsController::class, 'findMissingValue']);
    Route::get('intervals', [AlgorithmsController::class, 'intervals']);
    Route::get('anagrams', [AlgorithmsController::class, 'anagrams']);
    Route::get('islands', [AlgorithmsController::class, 'islands']);
});