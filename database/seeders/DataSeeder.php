<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Cart;
use App\Models\Product;
use App\Models\ProductGroupItem;
use App\Models\UserProductGroup;
use Illuminate\Database\Seeder;

class DataSeeder extends Seeder
{
    public function run(): void
    {
        $productModel = new Product();
        $userProductGroupModel = new UserProductGroup();
        $productGroupItemModel = new ProductGroupItem();
        $products = $productModel->whereIn('id', [2, 3])->get();
        $cartModel = new Cart();

        $userProductGroup = $userProductGroupModel->factory()->times(1)->create();
        $quantity = 2;
        foreach ($products as $product) {
            print_r('asdsad');
            $productGroupItemModel->factory()->times(1)->create(
                [
                    'group_id'   => $userProductGroup->first()->getId(),
                    'product_id' => $product->getId(),
                ],
            );

            $cartModel->factory()->times(1)->create(
                [
                    'product_id' => $product->getId(),
                    'quantity'   => $quantity,
                ],
            );

            $quantity++;
        }
    }
}