<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    public function run(): void
    {
        $model = new User();
        $model->factory()->times(10)->create();

        $model->factory()->create([
            'name' => 'Test User',
            'email' => 'test@example.com',
        ]);
    }
}