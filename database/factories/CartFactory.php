<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Models\Product;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class CartFactory extends Factory
{
    public function definition(): array
    {
        return [
            'user_id' => static function () {
                return (new User())->orderBy('id', 'desc')->first()->getId();
            },
            'product_id' => static function () {
                return (new Product())->inRandomOrder()->first();
            },
            'quantity' => rand(2, 3),
        ];
    }
}
