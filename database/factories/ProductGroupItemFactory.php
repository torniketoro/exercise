<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Models\Product;
use App\Models\UserProductGroup;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductGroupItemFactory extends Factory
{
    public function definition(): array
    {
        return [
            'group_id' => static function () {
                return (new UserProductGroup())->first()->getId();
            },
            'product_id' => static function () {
                return (new Product())->inRandomOrder()->first();
            },
        ];
    }
}
