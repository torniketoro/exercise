<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserProductGroupFactory extends Factory
{
    public function definition(): array
    {
        return [
            'user_id' => static function () {
                return (new User())->first()->getId();
            },
            'discount' => 15,
        ];
    }
}
