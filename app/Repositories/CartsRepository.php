<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Contracts\CartsRepositoryContract;
use App\Models\Cart;
use App\Models\ProductGroupItem;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class CartsRepository implements CartsRepositoryContract
{
    public function findItems(array $filters): Collection
    {
        $model = $this->getModel();

        $items = $model
            ->with('productGroupItems.userProductGroup')
            ->with('product')
            ->filterByUserId($filters)
            ->orderBy('id', 'desc')
            ->get();

        return $items;
    }

    public function findItemsJoined(array $filters): Collection
    {
        $model = $this->getModel();

        return $model->joinTables($filters)->get();
    }
    public function create(array $data): Cart
    {
        $item = $this->getModel();
        $item->fill($data);
        $item->saveOrFail();

        return $item;
    }

    public function findOne(array $data): ?Cart
    {
        return $this->getModel()->where(
            [
                'product_id' => $data['product_id'],
                'user_id'    => $data['user_id'],
            ],
        )->first();
    }

    public function delete(Cart $item): void
    {
        $item->delete();
    }

    public function setQuantity(cart $item, array $data): Cart
    {
        $item->fill($data);
        $item->saveOrFail();

        return $item;
    }

    private function getModel(): Cart
    {
        return new Cart();
    }
}