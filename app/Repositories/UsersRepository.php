<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Contracts\UsersRepositoryContract;
use App\Models\User;

class UsersRepository implements UsersRepositoryContract
{
    public function findOne(int $id): ?User
    {
        return $this->getModel()->find($id);
    }

    private function getModel(): User
    {
        return new User();
    }
}