<?php

declare(strict_types=1);

namespace App\Services;

use App\Contracts\UsersRepositoryContract;
use App\Exceptions\ItemNotFoundException;
use App\Models\User;

class UsersService
{
    private UsersRepositoryContract $repository;
    public function __construct(
        UsersRepositoryContract $repository,
    ) {
        $this->repository = $repository;
    }
    public function findOne(int $id): ?User
    {
        $item = $this->repository->findOne($id);
        if (! $item) {
            throw new ItemNotFoundException();
        }

        return $item;
    }
}