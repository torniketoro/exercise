<?php

declare(strict_types=1);

namespace App\Services;

use App\Contracts\CartsRepositoryContract;
use App\Exceptions\HttpException;
use App\Models\Cart;
use Illuminate\Support\Collection;

class CartsService
{
    private $authUserId = 11;
    private CartsRepositoryContract $repository;
    public function __construct(
        CartsRepositoryContract $repository
    ) {
        $this->repository = $repository;
    }

    public function findItems(): Collection
    {
        return $this->repository->findItems(['user_id' => $this->authUserId]);
    }

    public function findItemsJoined(): Collection
    {
        return $this->repository->findItemsJoined(['user_id' => $this->authUserId]);
    }

    public function create(int $productId): ?Cart
    {
        if ($this->findOne($productId)) {
            throw new HttpException(__('product already exists'), 401);
        }

        $data = [
            'product_id' => $productId,
            'user_id'    => $this->authUserId,
        ];

        return $this->repository->create($data);
    }

    public function delete(int $productId): void
    {
        $item = $this->findOne($productId);
        if (is_null($item)) {
            throw new HttpException(__('product does not exist'), 401);
        }

        $this->repository->delete($item);
    }

    public function findOne(int $productId): ?cart
    {
        $data = [
            'product_id' => $productId,
            'user_id'    => $this->authUserId,
        ];

        return $this->repository->findOne($data);
    }

    public function setQuantity(int $productId, array $data): Cart
    {
        $item = $this->findOne($productId);

        if (is_null($item)) {
            throw new HttpException(__('product does not exist'), 401);
        }

        return $this->repository->setQuantity($item, $data);
    }
}