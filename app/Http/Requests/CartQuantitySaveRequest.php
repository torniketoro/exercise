<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CartQuantitySaveRequest extends FormRequest
{
    public function rules(): array
    {
        $rules = [
            'quantity'       => 'required|integer',
        ];

        return $rules;
    }
}
