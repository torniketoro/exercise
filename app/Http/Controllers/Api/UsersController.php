<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Services\UsersService;
use Illuminate\Http\JsonResponse;

class UsersController extends ApiController
{
    public function show(int $id, UsersService $service): JsonResponse
    {
        $item = $service->findOne($id);

        return response()->json($item);
    }
}