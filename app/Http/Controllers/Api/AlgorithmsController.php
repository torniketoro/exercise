<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api;

class AlgorithmsController extends ApiController
{
    public function findMissingValue()
    {
        $array = [1, 2, 4, 5, 3, 7, 6];

        sort($array);

        $n = count($array);
        for ($i = 0; $i < $n - 1; $i++) {
            if ($array[$i + 1] - $array[$i] > 1) {
                return $array[$i] + 1;
            }
        }

        return end($array) + 1;
    }

    public function intervals()
    {
        $intervals = [[1, 3], [2, 6], [8, 10], [15, 18]];

        if (empty($intervals)) {
            return [];
        }

        usort($intervals, function($a, $b) {
            return $a[0] - $b[0];
        });

        $merged = [$intervals[0]];

        foreach ($intervals as $interval) {
            [$currentStart, $currentEnd] = end($merged);
            [$newStart, $newEnd] = $interval;

            if ($currentEnd >= $newStart) {
                $merged[count($merged) - 1] = [$currentStart, max($currentEnd, $newEnd)];
            } else {
                $merged[] = $interval;
            }
        }

        return $merged;
    }

    public function anagrams()
    {
        $words = ["eat", "tea", "tan", "ate", "nat", "bat"];
        $anagramGroups = [];

        foreach ($words as $word) {
            $sortedWord = str_split($word);
            sort($sortedWord);
            $signature = implode('', $sortedWord);

            $anagramGroups[$signature][] = $word;
        }

        return array_values($anagramGroups);
    }

    public function islands()
    {
        $grid = [
            ["1", "1", "1", "1", "0"],
            ["1", "1", "0", "1", "0"],
            ["1", "1", "0", "0", "0"],
            ["0", "0", "0", "1", "0"]
        ];

        if (empty($grid) || empty($grid[0])) {
            return 0;
        }

        $numIslands = 0;
        for ($i = 0; $i < count($grid); $i++) {
            for ($j = 0; $j < count($grid[0]); $j++) {
                if ($grid[$i][$j] == "1") {
                    $numIslands++;
                    $this->dfs($i, $j, $grid);
                }
            }
        }

        return $numIslands;
    }

    function dfs($row, $col, &$grid) {
        if ($row < 0 || $col < 0 || $row >= count($grid) || $col >= count($grid[0]) || $grid[$row][$col] == "0") {
            return;
        }
        $grid[$row][$col] = "0";  // Mark the current cell as visited
        // Explore all four directions
        $this->dfs($row + 1, $col, $grid);
        $this->dfs($row - 1, $col, $grid);
        $this->dfs($row, $col + 1, $grid);
        $this->dfs($row, $col - 1, $grid);
    }
}