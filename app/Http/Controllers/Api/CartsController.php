<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Http\Collections\CartCollection;
use App\Http\Collections\JoinedCartCollection;
use App\Http\Requests\CartQuantitySaveRequest;
use App\Services\CartsService;
use Illuminate\Http\JsonResponse;

class CartsController extends ApiController
{
    public function index(CartsService $service): JsonResponse
    {
        $items = $service->findItems();

        return response()->json(
            (new CartCollection($items))
        );
    }

    public function indexJoined(CartsService $service): JsonResponse
    {
        $items = $service->findItemsJoined();
        return response()->json(
            (new JoinedCartCollection($items))
        );
    }

    public function create(int $productId, CartsService $service): JsonResponse
    {
        $item = $service->create($productId);

        return response()->json($item);
    }

    public function delete(int $productId, CartsService $service): JsonResponse
    {
        $service->delete($productId);

        return response()->json(['status' => 'ok']);
    }

    public function setQuantity(int $productId, CartQuantitySaveRequest $request, CartsService $service): JsonResponse
    {
        $data = $request->validated();

        $item = $service->setQuantity($productId, $data);

        return response()->json($item);
    }
}