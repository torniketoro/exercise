<?php

namespace App\Http\Collections;

use App\Http\Resources\JoinedCartResource;
use App\Support\Num;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Collection;

class JoinedCartCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return [
            'products' => $this->collection->map(function ($item) {
                return (new JoinedCartResource($item));
            }),
            'discount' => $this->getDiscount($this->collection),
        ];
    }

    private function getDiscount(Collection $items): float
    {
        $discount = 0;

        if ($items->isEmpty()) {
            return $discount;
        }

        $items = $items->groupBy('group_id');

        foreach ($items as $groups) {
            foreach ($groups as $group) {
                if ($group['group_id']) {
                    $quantity = $groups->min('quantity');
                    $discount += ($quantity * Num::toMajor($group['price'])) * $group['discount'] / 100;
                }
            }
        }

        return round($discount, 4);
    }
}