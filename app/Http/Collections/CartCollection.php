<?php

namespace App\Http\Collections;

use App\Http\Resources\CartResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Collection;

class CartCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return [
            'products' => $this->collection->map(function ($item) {
                return (new CartResource($item));
            }),
            'discount' => $this->getDiscount($this->collection),
        ];
    }

    private function getDiscount(Collection $items): float
    {
        $transformedArray = [];
        $discount = 0;

        if ($items->isEmpty()) {
            return $discount;
        }

        $items = $items->groupBy('productGroupItems.group_id');

        foreach ($items as $k => $productGroupItems) {
            if ($productGroupItems->first()->productGroupItems) {
                $subArray = [];
                $subArray[$k] = [
                    'quantity' => $productGroupItems->min('quantity'),
                    'discount' => $productGroupItems->first()->productGroupItems->userProductGroup->getDiscount(),
                    'data'     => [],
                ];

                foreach ($productGroupItems as $item) {
                    $subArray[$k]['data'][] = [
                        'price' => $item->product->getPriceTomajor(),
                        'cart_id' => $item->getId(),
                    ];
                }

                $transformedArray[] = $subArray;
            }
        }

        foreach ($transformedArray as $gourp) {
            foreach ($gourp as $groupItem) {
                $quantity = $groupItem['quantity'];
                $discountPercentage = $groupItem['discount'];
                foreach ($groupItem['data'] as $price) {
                    $discount += ($price['price'] * $quantity) * ($discountPercentage / 100);
                }
            }
        }

        return round($discount, 4);
    }
}