<?php

declare(strict_types=1);

namespace App\Contracts;

use App\Models\User;

interface UsersRepositoryContract
{
    public function findOne(int $id): ?User;
}