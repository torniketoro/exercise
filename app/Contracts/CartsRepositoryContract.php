<?php

declare(strict_types=1);

namespace App\Contracts;

use App\Models\Cart;
use Illuminate\Support\Collection;

interface CartsRepositoryContract
{
    public function findItems(array $filters): Collection;

    public function findItemsJoined(array $filters): Collection;

    public function create(array $data): Cart;

    public function delete(Cart $item): void;

    public function findOne(array $data): ?Cart;

    public function setQuantity(cart $item, array $data): Cart;
}