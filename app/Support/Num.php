<?php

declare(strict_types=1);

namespace App\Support;

use function array_flip;
use function floor;
use function is_null;
use function is_numeric;
use function number_format;
use function pow;
use function preg_replace;
use function round;
use function sprintf;
use function str_replace;
use function strlen;
use function strtoupper;
use function substr;

class Num
{
    public static function format($number, int $decimals = 0, ?string $decimalPoint = null, ?string $thousandsSeparator = null): string
    {
        $number = (float) $number;

        if (is_null($decimalPoint)) {
            $decimalPoint = self::getDecimalPoint();
        }

        if (is_null($thousandsSeparator)) {
            $thousandsSeparator = self::getThousandsSeparator();
        }

        return number_format($number, $decimals, $decimalPoint, $thousandsSeparator);
    }

    public static function toMinor($number): int
    {
        $number = str_replace([',', ' '], ['.', ''], (string) $number);
        $number *= 10000;

        return (int) round($number);
    }

    public static function toMajor($number): float
    {
        return round($number / 10000, 4);
    }

    public static function formatForDatabaseAmount($number): float
    {
        return self::format($number, 2) * 100;
    }

    public static function getThousandsSeparator(): string
    {
        return ',';
    }

    public static function getDecimalPoint(): string
    {
        return '.';
    }

    public static function bytesToHumanReadable(int $bytes, int $decimals = 2): string
    {
        $size = ['B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
        $factor = (int) floor((strlen((string) $bytes) - 1) / 3);

        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . $size[$factor];
    }

    public static function humanReadableToBytes(string $from): int
    {
        $units = ['B', 'KB', 'MB', 'GB', 'TB', 'PB'];
        $number = substr($from, 0, -2);
        $suffix = strtoupper(substr($from, -2));

        // B or no suffix
        if (is_numeric(substr($suffix, 0, 1))) {
            return (int) preg_replace('/[^\d]/', '', $from);
        }

        $exponent = array_flip($units)[$suffix] ?? null;
        if ($exponent === null) {
            throw new InvalidArgumentException('Does not found exponent for string ' . $from);
        }

        return $number * (1024 ** $exponent);
    }
}
