<?php

declare(strict_types=1);

namespace App\Providers;

use App\Contracts\CartsRepositoryContract;
use App\Contracts\UsersRepositoryContract;
use App\Repositories\CartsRepository;
use App\Repositories\UsersRepository;
use Illuminate\Support\ServiceProvider;

class BindingsServiceProvider extends ServiceProvider
{
    private const REPOSITORIES = [
        UsersRepositoryContract::class                            => [
            UsersRepository::class,
        ],
        CartsRepositoryContract::class                            => [
            CartsRepository::class,
        ],
    ];

    public function boot(): void
    {
        //
    }

    public function register(): void
    {
        $cacheServices = config('project.general.cache_services');
        foreach (self::REPOSITORIES as $abstract => $repositories) {
            $concrete = $cacheServices ? ($repositories[1] ?? $repositories[0]) : $repositories[0];
            $this->app->bind($abstract, $concrete);
        }
    }
}