<?php

declare(strict_types=1);

namespace App\Models;

use App\Models\Model as BaseModel;
use App\Support\Num;

class Product extends BaseModel
{
    public function getId(): int
    {
        return $this->id;
    }

    public function getUserId(): int
    {
        return $this->user_id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    public function getPriceToMajor(): float
    {
        return Num::toMajor($this->getPrice());
    }

    public function getCreatedAt(): Carbon
    {
        return $this->created_at;
    }

    public function getUpdatedAt(): Carbon
    {
        return $this->updated_at;
    }
}
