<?php

declare(strict_types=1);

namespace App\Models;

use App\Models\Model as BaseModel;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
class ProductGroupItem extends BaseModel
{
    public function getId(): int
    {
        return $this->id;
    }

    public function getGroupId(): int
    {
        return $this->group_id;
    }

    public function getProductId(): string
    {
        return $this->product_id;
    }

    public function getCreatedAt(): Carbon
    {
        return $this->created_at;
    }

    public function getUpdatedAt(): Carbon
    {
        return $this->updated_at;
    }

    public function userProductGroup(): BelongsTo
    {
        return $this->belongsTo(UserProductGroup::class, 'group_id', 'id');
    }
}
