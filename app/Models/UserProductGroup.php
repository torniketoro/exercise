<?php

declare(strict_types=1);

namespace App\Models;

use App\Models\Model as BaseModel;

class UserProductGroup extends BaseModel
{
    public function getId(): int
    {
        return $this->id;
    }

    public function getUserId(): int
    {
        return $this->user_id;
    }

    public function getDiscount(): float
    {
        return (float) $this->discount;
    }

    public function getCreatedAt(): Carbon
    {
        return $this->created_at;
    }

    public function getUpdatedAt(): Carbon
    {
        return $this->updated_at;
    }
}
