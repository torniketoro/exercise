<?php

declare(strict_types=1);

namespace App\Models;

use App\Models\Model as BaseModel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\DB;

/** @method \App\Models\Cart filterByUserId(array $filters)
 * @method \App\Models\Cart joinTables(array $filters)*/

class Cart extends BaseModel
{
    public function getId(): int
    {
        return $this->id;
    }

    public function getUserId(): int
    {
        return $this->user_id;
    }

    public function getProductId(): float
    {
        return $this->product_id;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function getCreatedAt(): Carbon
    {
        return $this->created_at;
    }

    public function getUpdatedAt(): Carbon
    {
        return $this->updated_at;
    }

    public function productGroupItems(): BelongsTo
    {
        return $this->belongsTo(ProductGroupItem::class, 'product_id', 'product_id');
    }

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }

    public function scopeFilterByUserId(Builder $builder, array $filters): Builder
    {
        return $builder->when(isset($filters['user_id']), static function (Builder $query) use ($filters) {
            $query->where('user_id', $filters['user_id']);
        });
    }

    public function scopeJoinTables(Builder $builder, array $filters): Builder
    {
        return $builder
            ->select(
        'carts.quantity',
                'carts.user_id',
                'p.id as product_id',
                'p.title',
                'p.price',
                'discount_prod.discount',
                'discount_prod.group_id'
            )->join('products as p', 'p.id', '=', 'carts.product_id')
            ->leftJoin(DB::raw('(SELECT pgi.product_id, pgi.group_id, upg.discount
                        FROM product_group_items pgi
                        JOIN user_product_groups upg ON pgi.group_id=upg.id) as discount_prod'),
            'discount_prod.product_id', '=', 'carts.product_id')
            ->where('carts.user_id', $filters['user_id']);
    }
}
