<?php

declare(strict_types=1);

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Request;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

use function array_merge;

class HttpException extends Exception
{
    public function report(): void
    {
        try {
            $logger = app(LoggerInterface::class);
        } catch (Throwable $ex) {
            throw $this;
        }

        $logger->notice(
            $this->getMessage(),
            array_merge(
                $this->context(),
                ['exception' => $this],
            ),
        );
    }

    public function render(Request $request): Response
    {
        return $request->wantsJson()
            ? response()->json(['message' => $this->getMessage()], $this->getCode())
            : response($this->getMessage(), $this->getCode());
    }

    protected function context(): array
    {
        /*try {
            $user = app(AuthUserContract::class);
            if (! $user) {
                return [
                    'userId' => null,
                    'email'  => null,
                ];
            }

            return array_filter([
                'userId' => $user->getId(),
                'email'  => $user->getEmail(),
            ]);
        } catch (Throwable $e) {
            return [
                'userId' => null,
                'email'  => null,
                'error'  => $e->getMessage(),
            ];
        }*/

        return [];
    }
}
