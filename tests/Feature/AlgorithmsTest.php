<?php

declare(strict_types=1);

namespace Feature;

use Tests\TestCase as BaseAppTestCase;

class AlgorithmsTest extends BaseAppTestCase
{
    public function test_it_should_return_integer(): void
    {
        $response = $this->get('/api/algorithms/find-missing-value');
        dd($response->getContent());
    }

    public function test_it_should_return_array(): void
    {
        $response = $this->get('/api/algorithms/intervals');
        dd($response->getContent());
    }

    public function test_anagrams(): void
    {
        $response = $this->get('/api/algorithms/anagrams');
        dd($response->getContent());
    }

    public function test_islands(): void
    {
        $response = $this->get('/api/algorithms/islands');
        dd($response->getContent());
    }
}