<?php

declare(strict_types=1);

namespace Feature;

use Tests\TestCase as BaseAppTestCase;

class CartTest extends BaseAppTestCase
{

    public function test_it_should_create_item(): void
    {
        $response = $this->post('/api/carts/' . 1);
        dd($this->getDecodedContent($response->getContent()));
    }

    public function test_it_should_delete_item(): void
    {
        $response = $this->delete('/api/carts/' . 1);
        dd($this->getDecodedContent($response->getContent()));
    }

    public function test_it_should_set_quantity_for_item(): void
    {
        $data = [
            'quantity' => 5,
        ];
        $response = $this->patch('/api/carts/set-quantity/' . 1, $data);
        dd($this->getDecodedContent($response->getContent()));
    }

    public function test_it_should_show_items(): void
    {
        $response = $this->get('/api/carts/');
        dd($this->getDecodedContent($response->getContent()));
    }

    public function test_it_should_show_joined_items(): void
    {
        $response = $this->get('/api/carts/joined');
        dd($this->getDecodedContent($response->getContent()));
    }

    private function getDecodedContent(string $content)
    {
        return json_decode($content, true);
    }
}